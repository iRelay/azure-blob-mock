package net.relaysoft.testing.azure.blob.mock.exceptions

import scala.xml.Elem

case class InternalErrorException(throwable: Throwable) extends Exception(s"Internal server error", throwable) {
  def toXML: Elem =
    <Error>
      <Code>InternalError</Code>
      <Message>{throwable.getMessage}</Message>
    </Error>
}
