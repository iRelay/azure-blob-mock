package net.relaysoft.testing.azure.blob.mock.exceptions

import scala.xml.Elem

case class ContainerNotFoundException() extends Exception(s"The specified container does not exist.") {
  def toXML: Elem =
    <Error>
      <Code>ContainerNotFound</Code>
      <Message>{getMessage}</Message>
    </Error>
}
