package net.relaysoft.testing.azure.blob.mock.utils

object HeaderNames {
  val CONTENT_ENCODING = "Content-Encoding"
  val CONTENT_LANGUAGE = "Content-Language"
  val CONTENT_LENGTH = "Content-Length"
  val CONTENT_MD5 = "Content-MD5"
  val CONTENT_TYPE = "Content-Type"

  val DATE = "Date"

  val ETAG = "ETag"

  val LAST_MODIFIED = "Last-Modified"

  val PREFIX_X_MS_META = "x-ms-meta-"

  val X_MS_BLOB_TYPE = "x-ms-blob-type"
  val X_MS_CLIENT_REQUEST_ID = "x-ms-client-request-id"
  val X_MS_CREATION_TIME = "x-ms-creation-time"
  val X_MS_REQUEST_ID = "x-ms-request-id"
  val X_MS_REQUEST_SERVER_ENCRYPTED = "x-ms-request-server-encrypted"
  val X_MS_SERVER_ENCRYPTED = "x-ms-server-encrypted"
  val X_MS_VERSION = "x-ms-version"
}
