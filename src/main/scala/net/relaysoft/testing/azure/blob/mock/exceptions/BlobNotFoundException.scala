package net.relaysoft.testing.azure.blob.mock.exceptions

import scala.xml.Elem

case class BlobNotFoundException() extends Exception(s"The specified blob does not exist.") {
  def toXML: Elem =
    <Error>
      <Code>BlobNotFound</Code>
      <Message>{getMessage}</Message>
    </Error>
}
