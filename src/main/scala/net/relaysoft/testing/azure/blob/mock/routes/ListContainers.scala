package net.relaysoft.testing.azure.blob.mock.routes

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{ContentType, DateTime, HttpCharsets, HttpEntity, HttpResponse, MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import net.relaysoft.testing.azure.blob.mock.exceptions.InternalErrorException
import net.relaysoft.testing.azure.blob.mock.providers.Provider
import net.relaysoft.testing.azure.blob.mock.utils.HeaderNames

import java.util.UUID
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

/**
 * The List Containers route operation returns a list of the containers from configured mock provider under the test
 * storage account.
 *
 * @param provider Mock provider
 */
case class ListContainers()(implicit provider:Provider) extends LazyLogging {

  def route(account:String, params: Map[String, String], headers: Map[String, String]): Route = get {
    parameters(Symbol("comp") ! "list") {
      complete {
        val paramString = params.map(_.productIterator.mkString(":")).mkString(",")
        val headerString = headers.map(_.productIterator.mkString(":")).mkString(",")
        logger.debug(s"account=$account, message=Listing all containers from account, " +
          s"params=[$paramString], headers=[$headerString]")
        Try(provider.listContainers(params)) match {
          case Success(containers) => HttpResponse(
            StatusCodes.OK,
            entity = HttpEntity(
              ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
              containers.toXML.toString
            )
          ).withHeaders(
            getResponseHeaders(headers)
          )
          case Failure(e) => HttpResponse(
            StatusCodes.InternalServerError,
            entity = HttpEntity(
              ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
              InternalErrorException(e).toXML.toString
            )
          )
        }
      }
    }
  }

  def getResponseHeaders(requestHeaders: Map[String, String]): Seq[RawHeader] = {
    val headers = Seq(
      RawHeader(HeaderNames.X_MS_REQUEST_ID, UUID.randomUUID().toString),
      RawHeader(HeaderNames.X_MS_VERSION, requestHeaders.getOrElse(HeaderNames.X_MS_VERSION, "")),
      RawHeader(HeaderNames.DATE, DateTime.now.toRfc1123DateTimeString()))
    if(requestHeaders.contains(HeaderNames.X_MS_CLIENT_REQUEST_ID)) {
      val clientHeaders = Seq(RawHeader(HeaderNames.X_MS_CLIENT_REQUEST_ID,
        requestHeaders.getOrElse(HeaderNames.X_MS_CLIENT_REQUEST_ID, "")))
      return headers ++ clientHeaders
    }
    headers
  }
}
