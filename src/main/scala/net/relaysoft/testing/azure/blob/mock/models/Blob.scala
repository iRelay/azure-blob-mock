package net.relaysoft.testing.azure.blob.mock.models

import akka.http.scaladsl.model.DateTime

/**
 * Blob data model.
 *
 * NOTE: Does NOT contain the actual blob content.
 *
 * @param name Blob name
 * @param lastModifiedDate Blob last modified time
 * @param etag Entity tag associated with the blob
 * @param contentLength Blob content length
 * @param contentType Blob content MIME type
 * @param contentEncoding Blob content encoding type
 * @param contentLanguage Blob content language
 * @param contentMD5 Blob content MD5 hash
 * @param cacheControl Blob cache control
 * @param blobType Blob type
 * @param leaseStatus Blob lease status
 * @param metaData Blob metadata
 */
case class Blob(name:String,
                creationDate:DateTime = DateTime.now,
                lastModifiedDate:DateTime = DateTime.now,
                etag:String = "",
                contentLength:Long = 0,
                contentType:String = "",
                contentEncoding:String = "",
                contentLanguage:String = "",
                contentMD5:String = "",
                cacheControl:String = "no-cache",
                blobType:String = "BlockBlob",
                leaseStatus:String = "unlocked",
                metaData:Map[String, String] = Map.empty[String, String],
                content:Array[Byte] = Array[Byte]())
