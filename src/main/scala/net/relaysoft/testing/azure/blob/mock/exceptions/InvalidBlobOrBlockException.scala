package net.relaysoft.testing.azure.blob.mock.exceptions

import scala.xml.Elem

case class InvalidBlobOrBlockException() extends Exception(s"The specified blob or block content is invalid.") {
  def toXML: Elem =
    <Error>
      <Code>InvalidBlobOrBlock</Code>
      <Message>{getMessage}</Message>
    </Error>
}