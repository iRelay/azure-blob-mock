package net.relaysoft.testing.azure.blob.mock.providers

import net.relaysoft.testing.azure.blob.mock.models._
import net.relaysoft.testing.azure.blob.mock.responses._

/**
 * Interface for the Azure Blob Mock provider implementations. Provider imitates a single Azure storage account.
 */
trait Provider {

  /**
   * Removes all blobs and containers from storage account.
   */
  def clear(): Unit

  /**
   * Delete blob from storage data container.
   *
   * @param containerName Container name
   * @param blobName Blob name to delete
   */
  def deleteBlob(containerName:String, blobName:String): Unit

  /**
   * Delete container from storage account.
   *
   * @param containerName Container name to delete
   */
  def deleteContainer(containerName:String): Unit

  /**
   * Create new storage data container.
   *
   * @param containerName Container name
   * @return Created container model.
   */
  def createContainer(containerName: String): Container

  /**
   * Get blob from storage data container.
   *
   * @param containerName Container name
   * @param blobName Blob name
   * @param headers Request headers
   * @return Blob model.
   */
  def getBlob(containerName:String, blobName:String, headers: Map[String, String]): Blob

  /**
   * Get properties for the storage data container.
   *
   * @param containerName Container name
   * @return Container model.
   */
  def getContainerProperties(containerName:String): Container

  /**
   * Listing blobs from storage data container.
   *
   * Blobs can be filtered out with request parameter "prefix". When used, method only list blobs which name start with
   * the given prefix value.
   *
   * @param containerName Container name
   * @param params Request parameters
   * @return List blobs response model.
   */
  def listBlobs(containerName:String, params: Map[String, String]): ListBlobsResponse

  /**
   * Listing storage data containers from storage account.
   *
   * Containers can be filtered out with request parameter "prefix". When used, method only list containers which name
   * start with the given prefix value.
   *
   * @param params Request parameters
   * @return List containers response model.
   */
  def listContainers(params: Map[String, String]): ListContainersResponse

  /**
   * Create new or update existing blob in storage data container.
   *
   * Supports following request parameters:
   * <ul>
   *   <li>Content-Length</li>
   *   <li>Content-Type</li>
   *   <li>Content-Encoding</li>
   *   <li>Content-Language</li>
   * </ul>
   *
   * @param containerName Container name
   * @param blobName Blob name
   * @param data Blob data content
   * @param params Request params
   * @return Saved blob model.
   */
  def putBlob(containerName:String, blobName:String, data:Array[Byte], params: Map[String, String]): Blob

  /**
   * Update existing blob meta-data.
   *
   * @param containerName Container name
   * @param blobName Blob name
   * @param params Request params containing meta-data
   */
  def setBlobMetadata(containerName:String, blobName:String, params: Map[String, String]): Blob

}
