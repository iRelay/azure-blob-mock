package net.relaysoft.testing.azure.blob.mock.routes

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{complete, put}
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import net.relaysoft.testing.azure.blob.mock.exceptions.{ContainerNotFoundException, InternalErrorException, InvalidBlobOrBlockException}
import net.relaysoft.testing.azure.blob.mock.models.Blob
import net.relaysoft.testing.azure.blob.mock.providers.Provider
import net.relaysoft.testing.azure.blob.mock.utils.HeaderNames

import scala.util.{Failure, Success, Try}

case class PutBlob()(implicit provider:Provider) extends LazyLogging {

  def route(accountName: String, containerName:String, blobName:String, data:Array[Byte],
            headers: Map[String, String]): Route = put {
    complete {
      val headerString = headers.map(_.productIterator.mkString(":")).mkString(",")
      logger.debug(s"account=$accountName, container=$containerName, blob=$blobName, message=Create or update blob, " +
        s"headers=[$headerString]")
      Try(provider.putBlob(containerName, blobName, data, headers)) match {
        case Success(blob) => HttpResponse(
          StatusCodes.Created
        ).withHeaders(
          getResponseHeaders(blob, headers)
        )
        case Failure(e: InvalidBlobOrBlockException) => HttpResponse(
          StatusCodes.BadRequest,
          entity = HttpEntity(
            ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
            e.toXML.toString
          )
        )
        case Failure(e: ContainerNotFoundException) => HttpResponse(
          StatusCodes.NotFound,
          entity = HttpEntity(
            ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
            e.toXML.toString
          )
        )
        case Failure(e) => HttpResponse(
          StatusCodes.InternalServerError,
          entity = HttpEntity(
            ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
            InternalErrorException(e).toXML.toString
          )
        )
      }
    }
  }

  def getResponseHeaders(blob: Blob, params: Map[String, String]): Seq[RawHeader] = {
    val headers = Seq(
      RawHeader(HeaderNames.ETAG, blob.etag),
      RawHeader(HeaderNames.LAST_MODIFIED, blob.lastModifiedDate.toRfc1123DateTimeString()),
      RawHeader(HeaderNames.CONTENT_MD5, blob.contentMD5),
      RawHeader(HeaderNames.X_MS_VERSION, params.getOrElse(HeaderNames.X_MS_VERSION, "")),
      RawHeader(HeaderNames.X_MS_REQUEST_SERVER_ENCRYPTED, "false"))
    headers
  }
}
