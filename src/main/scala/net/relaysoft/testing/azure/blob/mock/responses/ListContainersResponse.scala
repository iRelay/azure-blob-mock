package net.relaysoft.testing.azure.blob.mock.responses

import net.relaysoft.testing.azure.blob.mock.models.Container

import scala.xml.Elem

case class ListContainersResponse(baseUrl:String, containers:List[Container], prefix:String = "") {
  def toXML: Elem =
    <EnumerationResults ServiceEndpoint={baseUrl}>
      <Prefix>{prefix}</Prefix>
      <Containers>
        {
        containers.map(container =>
        <Container>
          <Name>{container.name}</Name>
          <Deleted>{container.deleted}</Deleted>
          <Properties>
            <Last-Modified>{container.lastModifiedDate.toRfc1123DateTimeString()}</Last-Modified>
            <Etag>{container.etag}</Etag>
          </Properties>
          <Metadata>
            {container.metaData.map{ case (name:String, value:String) =>
            {<x>.copy(label = name){value}</x>.copy(label = name)}
          }
            }
          </Metadata>
        </Container>
        )}
      </Containers>
    </EnumerationResults>
}
