package net.relaysoft.testing.azure.blob.mock.routes

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{complete, get}
import akka.http.scaladsl.server.Route
import akka.util.ByteString
import com.typesafe.scalalogging.LazyLogging
import net.relaysoft.testing.azure.blob.mock.exceptions._
import net.relaysoft.testing.azure.blob.mock.models.Blob
import net.relaysoft.testing.azure.blob.mock.providers.Provider
import net.relaysoft.testing.azure.blob.mock.utils.HeaderNames

import scala.util.{Failure, Success, Try}

case class GetBlob()(implicit provider:Provider) extends LazyLogging {

  def route(account: String, containerName:String, blobName:String, headers: Map[String, String]): Route = get {
    complete {
      val headerString = headers.map(_.productIterator.mkString(":")).mkString(",")
      logger.debug(s"account=$account, container=$containerName, blob=$blobName, message=Get blob, " +
        s"headers=[$headerString]")
      Try(provider.getBlob(containerName, blobName, headers)) match {
        case Success(blob) => HttpResponse(
          StatusCodes.OK,
          entity = HttpEntity(
            ContentType(MediaType.customWithOpenCharset(getMediaTypeMainPart(blob),getMediaTypeSubPart(blob)),
              HttpCharsets.getForKey(getCharset(blob)).getOrElse(HttpCharsets.`UTF-8`)),
            ByteString.apply(blob.content)
          )
        ).withHeaders(
          getResponseHeaders(blob, headers)
        )
        case Failure(e: InvalidBlobOrBlockException) => HttpResponse(
          StatusCodes.BadRequest,
          entity = HttpEntity(
            ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
            e.toXML.toString
          )
        )
        case Failure(e: ContainerNotFoundException) => HttpResponse(
          StatusCodes.NotFound,
          entity = HttpEntity(
            ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
            e.toXML.toString
          )
        )
        case Failure(e: BlobNotFoundException) => HttpResponse(
          StatusCodes.NotFound,
          entity = HttpEntity(
            ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
            e.toXML.toString
          )
        )
        case Failure(e) => HttpResponse(
          StatusCodes.InternalServerError,
          entity = HttpEntity(
            ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
            InternalErrorException(e).toXML.toString
          )
        )
      }
    }
  }

  def getResponseHeaders(blob: Blob, params: Map[String, String]): Seq[RawHeader] = {
    val metadataHeaders = blob.metaData.keySet.map(key => RawHeader(HeaderNames.PREFIX_X_MS_META + key,
      blob.metaData.getOrElse(key, ""))).toSeq
    val headers = Seq(
      RawHeader(HeaderNames.ETAG, blob.etag),
      RawHeader(HeaderNames.LAST_MODIFIED, blob.lastModifiedDate.toRfc1123DateTimeString()),
      RawHeader(HeaderNames.CONTENT_MD5, blob.contentMD5),
      RawHeader(HeaderNames.X_MS_VERSION, params.getOrElse(HeaderNames.X_MS_VERSION, "")),
      RawHeader(HeaderNames.X_MS_SERVER_ENCRYPTED, "false"))
    headers ++ metadataHeaders
  }

  def getMediaTypeMainPart(blob: Blob): String = {
    val parts = getMediaType(blob).split("/")
    if(parts != null && parts.nonEmpty) return parts(0)
    ""
  }

  def getMediaTypeSubPart(blob: Blob): String = {
    val parts = getMediaType(blob).split("/")
    if(parts != null && parts.nonEmpty) return parts(1)
    ""
  }

  def getMediaType(blob: Blob): String = {
    val values = blob.contentType.split(";")
    if(values != null && values.nonEmpty) return values(0)
    "application/octet-stream"
  }

  def getCharset(blob: Blob): String = {
    val values = blob.contentType.split(";")
    if(values != null && values.length > 1) return values(1)
    "UTF-8"
  }

}


