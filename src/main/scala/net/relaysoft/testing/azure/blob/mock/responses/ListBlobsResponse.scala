package net.relaysoft.testing.azure.blob.mock.responses

import net.relaysoft.testing.azure.blob.mock.models.Blob

import scala.xml.Elem

/**
 * List blob query response model.
 *
 * @param baseUrl Request base URL
 * @param blobs Blobs to return
 */
case class ListBlobsResponse(baseUrl:String, blobs:List[Blob]) {
  def toXML: Elem =
    <EnumerationResults ContainerName={baseUrl}>
      <Blobs>
        {
        blobs.map(blob =>
        <Blob>
        <Name>{blob.name}</Name>
        <Url>{baseUrl}/{blob.name}</Url>
        <Properties>
          <Last-Modified>{blob.lastModifiedDate.toRfc1123DateTimeString()}</Last-Modified>
          <Etag>{blob.etag}</Etag>
          <Content-Length>{blob.contentLength}</Content-Length>
          <Content-Type>{blob.contentType}</Content-Type>
          <Content-Encoding>{blob.contentEncoding}</Content-Encoding>
          <Content-Language>{blob.contentLanguage}</Content-Language>
          <Content-MD5>{blob.contentMD5}</Content-MD5>
          <Cache-Control>{blob.cacheControl}</Cache-Control>
          <BlobType>{blob.blobType}</BlobType>
          <LeaseStatus>{blob.leaseStatus}</LeaseStatus>
        </Properties>
        <Metadata>
          {blob.metaData.map{ case (name:String, value:String) =>
            {<x>.copy(label = name){value}</x>.copy(label = name)}
          }
          }
        </Metadata>
        </Blob>)
        }
      </Blobs>
    </EnumerationResults>
}
