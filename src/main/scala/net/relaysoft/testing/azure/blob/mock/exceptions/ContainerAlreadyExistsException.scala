package net.relaysoft.testing.azure.blob.mock.exceptions

import scala.xml.Elem

case class ContainerAlreadyExistsException() extends Exception(s"The specified container already exists.") {
  def toXML: Elem =
    <Error>
      <Code>ContainerAlreadyExists</Code>
      <Message>{getMessage}</Message>
    </Error>
}
