package net.relaysoft.testing.azure.blob.mock.routes

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import net.relaysoft.testing.azure.blob.mock.exceptions._
import net.relaysoft.testing.azure.blob.mock.models.Blob
import net.relaysoft.testing.azure.blob.mock.providers.Provider
import net.relaysoft.testing.azure.blob.mock.utils.HeaderNames

import java.util.UUID
import scala.util.{Failure, Success, Try}

case class GetBlobMetadata()(implicit provider:Provider) extends LazyLogging {
  def route(account: String, containerName:String, blobName:String, headers: Map[String, String]): Route = get {
    parameters(Symbol("comp") ! "metadata") {
      complete {
        val headerString = headers.map(_.productIterator.mkString(":")).mkString(",")
        logger.debug(s"account=$account, container=$containerName, blob=$blobName, message=Get blob meta-data, " +
          s"headers=[$headerString]")
        Try(provider.getBlob(containerName, blobName, headers)) match {
          case Success(blob) => HttpResponse(StatusCodes.OK).withHeaders(getResponseHeaders(blob, headers))
          case Failure(e: InvalidBlobOrBlockException) => HttpResponse(
            StatusCodes.BadRequest,
            entity = HttpEntity(
              ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
              e.toXML.toString
            )
          )
          case Failure(e: ContainerNotFoundException) => HttpResponse(
            StatusCodes.NotFound,
            entity = HttpEntity(
              ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
              e.toXML.toString
            )
          )
          case Failure(e: BlobNotFoundException) => HttpResponse(
            StatusCodes.NotFound,
            entity = HttpEntity(
              ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
              e.toXML.toString
            )
          )
          case Failure(e) => HttpResponse(
            StatusCodes.InternalServerError,
            entity = HttpEntity(
              ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
              InternalErrorException(e).toXML.toString
            )
          )
        }
      }
    }
  }

  def getResponseHeaders(blob: Blob, requestHeaders: Map[String, String]): Seq[RawHeader] = {
    val metadataHeaders = blob.metaData.keySet.map(key => RawHeader(HeaderNames.PREFIX_X_MS_META + key,
      blob.metaData.getOrElse(key, ""))).toSeq
    val headers = Seq(
      RawHeader(HeaderNames.ETAG, blob.etag),
      RawHeader(HeaderNames.LAST_MODIFIED, blob.lastModifiedDate.toRfc1123DateTimeString()),
      RawHeader(HeaderNames.DATE, DateTime.now.toRfc1123DateTimeString()),
      RawHeader(HeaderNames.CONTENT_MD5, blob.contentMD5),
      RawHeader(HeaderNames.X_MS_REQUEST_ID, UUID.randomUUID().toString),
      RawHeader(HeaderNames.X_MS_VERSION, requestHeaders.getOrElse(HeaderNames.X_MS_VERSION, "")),
      RawHeader(HeaderNames.X_MS_SERVER_ENCRYPTED, "false"))
    if(requestHeaders.contains(HeaderNames.X_MS_CLIENT_REQUEST_ID)) {
      val clientHeaders = Seq(RawHeader(HeaderNames.X_MS_CLIENT_REQUEST_ID,
        requestHeaders.getOrElse(HeaderNames.X_MS_CLIENT_REQUEST_ID, "")))
      return headers ++ metadataHeaders ++ clientHeaders
    }
    headers ++ metadataHeaders
  }
}
