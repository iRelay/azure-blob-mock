package net.relaysoft.testing.azure.blob.mock.models

import akka.http.scaladsl.model.DateTime

/**
 * Container data model.
 *
 * @param name Container name
 * @param lastModifiedDate Container last modified time
 * @param etag Entity tag associated with the container
 * @param deleted Marks container deleted status
 * @param metaData Container metadata
 */
case class Container(name:String,
                     lastModifiedDate:DateTime = DateTime.now,
                     etag:String = "",
                     deleted:Boolean = false,
                     metaData:Map[String, String] = Map.empty[String, String])
