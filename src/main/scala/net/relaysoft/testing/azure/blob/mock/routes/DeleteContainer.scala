package net.relaysoft.testing.azure.blob.mock.routes

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import net.relaysoft.testing.azure.blob.mock.exceptions.{ContainerNotFoundException, InternalErrorException}
import net.relaysoft.testing.azure.blob.mock.providers.Provider
import net.relaysoft.testing.azure.blob.mock.utils.HeaderNames

import java.util.UUID
import scala.util.{Failure, Success, Try}

case class DeleteContainer()(implicit provider:Provider) extends LazyLogging {
  def route(accountName: String, containerName:String, headers: Map[String, String]): Route = delete {
    parameters(Symbol("restype") ! "container") {
      complete {
        val headerString = headers.map(_.productIterator.mkString(":")).mkString(",")
        logger.debug(s"account=$accountName, container=$containerName, message=Deleting blob, " +
          s"headers=[$headerString]")
        Try(provider.deleteContainer(containerName)) match {
          case Success(_) => HttpResponse(StatusCodes.Accepted).withHeaders(getResponseHeaders(headers))
          case Failure(e: ContainerNotFoundException) => HttpResponse(
            StatusCodes.NotFound,
            entity = HttpEntity(
              ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
              e.toXML.toString
            )
          )
          case Failure(e) => HttpResponse(
            StatusCodes.InternalServerError,
            entity = HttpEntity(
              ContentType(MediaTypes.`application/xml`, HttpCharsets.`UTF-8`),
              InternalErrorException(e).toXML.toString
            )
          )
        }
      }
    }
  }

  def getResponseHeaders(requestHeaders: Map[String, String]): Seq[RawHeader] = {
    val headers = Seq(
      RawHeader(HeaderNames.DATE, DateTime.now.toRfc1123DateTimeString()),
      RawHeader(HeaderNames.X_MS_REQUEST_ID, UUID.randomUUID().toString),
      RawHeader(HeaderNames.X_MS_VERSION, requestHeaders.getOrElse(HeaderNames.X_MS_VERSION, "")))
    if(requestHeaders.contains(HeaderNames.X_MS_CLIENT_REQUEST_ID)) {
      val clientHeaders = Seq(RawHeader(HeaderNames.X_MS_CLIENT_REQUEST_ID,
        requestHeaders.getOrElse(HeaderNames.X_MS_CLIENT_REQUEST_ID, "")))
      return headers ++ clientHeaders
    }
    headers
  }
}
