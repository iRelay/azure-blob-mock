package net.relaysoft.testing.azure.blob.mock

import com.azure.storage.blob.models.BlobStorageException

class GetContainerPropertiesTest extends AzureBlobMockTest {
  override def behaviour(fixture: => Fixture): Unit = {

    val client = fixture.client
    val containerName = "test"

    it should "fail to get non-existing container properties" in {
      assertThrows[BlobStorageException] {
        client.getBlobContainerClient(containerName).getProperties()
      }
    }

    it should "verify container does not exists" in {
      client.getBlobContainerClient(containerName).exists() shouldBe false
    }

    it should "get container properties" in {
      client.createBlobContainer(containerName)
      val result = client.getBlobContainerClient(containerName).getProperties()
      result.getETag should not be empty
      result.getLastModified should not be null
    }

    it should "verify container existence" in {
      client.createBlobContainer(containerName)
      client.getBlobContainerClient(containerName).exists() shouldBe true
    }

  }
}
