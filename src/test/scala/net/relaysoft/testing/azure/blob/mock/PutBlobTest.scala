package net.relaysoft.testing.azure.blob.mock

import java.io.{BufferedOutputStream, ByteArrayInputStream, ByteArrayOutputStream, OutputStream}
import scala.jdk.CollectionConverters._

class PutBlobTest extends AzureBlobMockTest {

  val containerName = "testContainer"

  override def beforeEach = {
    super.beforeEach()
    createContainer(containerName)
  }

  override def behaviour(fixture: => Fixture) = {

    val client = fixture.client
    val blobName = "testBlob"
    val blobContent = "blob content".getBytes("UTF-8")

    it should "upload new blob" in {
      val result = client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient
        .upload(new ByteArrayInputStream(blobContent), blobContent.length, false)
      result.getETag should not be None
      val listResult = client.getBlobContainerClient(containerName).listBlobs().asScala.toList
      listResult.size shouldBe 1
      val names = listResult.map(blob => blob.getName).sorted
      names shouldEqual List(blobName).sorted
    }

    it should "get output stream to new blob" in {
      val os = client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient
        .getBlobOutputStream(false)
      writeBytes(blobContent, os)
      val listResult = client.getBlobContainerClient(containerName).listBlobs().asScala.toList
      listResult.size shouldBe 1
      val names = listResult.map(blob => blob.getName).sorted
      names shouldEqual List(blobName).sorted
    }

    it should "upload over existing blob" in {
      val original = createTestBlob(containerName, blobName, "xxx".getBytes("UTF-8"))
      val result = client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient
        .upload(new ByteArrayInputStream(blobContent), blobContent.length, true)
      result.getETag shouldBe original.getETag
      val listResult = client.getBlobContainerClient(containerName).listBlobs().asScala.toList
      listResult.size shouldBe 1
      val names = listResult.map(blob => blob.getName).sorted
      names shouldEqual List(blobName).sorted
    }

  }

  def writeBytes( data : Array[Byte], os: OutputStream) = {
    val target = new BufferedOutputStream( os );
    try data.foreach( target.write(_) ) finally target.close;
  }

}
