package net.relaysoft.testing.azure.blob.mock

import com.azure.storage.blob.models.BlobStorageException

class GetBlobPropertiesTest extends AzureBlobMockTest {

  val containerName = "testContainer"

  override def beforeEach: Unit = {
    super.beforeEach()
    createContainer(containerName)
  }

  override def behaviour(fixture: => Fixture): Unit = {

    val client = fixture.client
    val blobName = "testBlob"
    val blobContent = "blob content".getBytes("UTF-8")

    it should "fail to get non-existing blob properties" in {
      assertThrows[BlobStorageException] {
        client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient.getProperties
      }
    }

    it should "get blob properties" in {
      val original = createTestBlob(containerName, blobName, blobContent, Map("testKey" -> "testValue"))
      val props = client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient.getProperties
      props.getETag should be(original.getETag)
      props.getLastModified should be(original.getLastModified)
      props.getBlobSize should be(blobContent.length)
      val meta = props.getMetadata
      meta.size() shouldBe 1
      meta.get("testKey") should be("testValue")
    }
  }

}
