package net.relaysoft.testing.azure.blob.mock

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import com.azure.storage.blob.models.BlockBlobItem
import com.azure.storage.blob.specialized.BlockBlobClient
import com.azure.storage.blob.{BlobContainerClient, BlobServiceClient, BlobServiceClientBuilder}
import com.azure.storage.common.StorageSharedKeyCredential
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.LazyLogging
import net.relaysoft.testing.azure.blob.mock.providers.InMemoryProvider
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}

import java.io.ByteArrayInputStream
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.io.Source
import scala.jdk.CollectionConverters._
import scala.util.{Try, Using}

trait AzureBlobMockTest extends FlatSpec with Matchers with BeforeAndAfterAll with BeforeAndAfterEach with LazyLogging{

  protected val accountName = "devstoreaccount1"

  private val accountKey = "Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw=="

  private val inMemoryPort = 8002
  private val inMemoryConfig = configFor()
  private val inMemorySystem = ActorSystem.create("testAzureBlobMock", inMemoryConfig)
  private val inMemoryMat = ActorMaterializer()(inMemorySystem)
  private val inMemoryClient = clientFor("localhost", inMemoryPort, accountName, new StorageSharedKeyCredential(accountName, accountKey))
  private val inMemoryServer = new AzureBlobMock(inMemoryPort, accountName, new InMemoryProvider(inMemoryPort, accountName))

  case class Fixture(server: AzureBlobMock,
                     client: BlobServiceClient,
                     name: String,
                     port: Int,
                     system: ActorSystem,
                     mat: Materializer)

  val fixtures = List(
    Fixture(inMemoryServer, inMemoryClient, "in-memory Azure Blob Mock", inMemoryPort, inMemorySystem, inMemoryMat)
  )

  def behaviour(fixture: => Fixture) : Unit

  for (fixture <- fixtures) {
    fixture.name should behave like behaviour(fixture)
  }

  override def beforeAll: Unit = {
    inMemoryServer.start
    super.beforeAll
  }

  override def afterAll: Unit = {
    super.afterAll
    inMemoryServer.shutdown()
    Await.result(inMemorySystem.terminate(), Duration.Inf)
  }

  override def afterEach: Unit = {
    super.afterEach()
    inMemoryServer.clear()
  }

  def clientFor(host: String, port: Int, account:String, credentials: StorageSharedKeyCredential): BlobServiceClient = {
    val endpoint = s"http://$host:$port/$account"
    new BlobServiceClientBuilder()
      .endpoint(endpoint)
      .credential(credentials)
      .buildClient()
  }

  def configFor(): Config = {
    ConfigFactory.empty()
  }

  protected def createTestBlob(containerName: String, blobName: String, data: Array[Byte]): BlockBlobItem = {
    createTestBlob(containerName, blobName, data, Map[String, String]())
  }

  protected def createTestBlob(containerName: String, blobName: String, data: Array[Byte], metadata: Map[String, String]): BlockBlobItem = {
    val a: Try[BlockBlobItem] = Using(new ByteArrayInputStream(data)) { byteArrayInputStream =>
      getBlockBlobClient(containerName, blobName).upload(byteArrayInputStream, data.length, true)
    }
    if(metadata != null && metadata.nonEmpty){
      getBlockBlobClient(containerName, blobName).setMetadata(metadata.asJava)
    }
    a.get
  }

  def createContainer(containerName: String): BlobContainerClient = {
    inMemoryClient.createBlobContainer(containerName)
  }

  def getBlobContent(containerName: String, blobName: String): String = {
    getBlobContent(getBlockBlobClient(containerName, blobName))
  }

  /**
   * Get blob content as string.
   *
   * @param blockBlobClient Blob content client instance
   * @return Blob content as string.
   */
  private def getBlobContent(blockBlobClient: BlockBlobClient): String = {
    Source.fromInputStream(blockBlobClient.openInputStream(), "UTF-8").mkString
  }

  /**
   * Get Block Blob client instance.
   *
   * @param containerName Container name
   * @param blobName Blob name
   * @return Block Blob client instance.
   */
  private def getBlockBlobClient(containerName: String, blobName: String): BlockBlobClient = {
    inMemoryClient.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient
  }
}
