package net.relaysoft.testing.azure.blob.mock

import com.azure.storage.blob.models.BlobStorageException

import scala.jdk.CollectionConverters.MapHasAsJava

class SetBlobMetadataTest extends AzureBlobMockTest {

  val containerName = "testContainer"

  protected override def beforeEach: Unit = {
    super.beforeEach()
    createContainer(containerName)
  }

  override def behaviour(fixture: => Fixture): Unit = {

    val client = fixture.client
    val blobName = "testBlob"
    val blobContent = "blob content".getBytes("UTF-8")

    it should "fail to set non-existing blob meta-data" in {
      assertThrows[BlobStorageException] {
        client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient
          .setMetadata(getMetadataMap(Map("testKey" -> "testValue")))
      }
    }

    it should "set blob meta-data" in {
      val blobClient = client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient
      createTestBlob(containerName, blobName, blobContent)
      blobClient.getProperties.getMetadata.isEmpty shouldBe true
      blobClient.setMetadata(getMetadataMap(Map("testKey" -> "testValue")))
      blobClient.getProperties.getMetadata.size() shouldBe 1
      blobClient.getProperties.getMetadata.get("testKey") should be("testValue")
    }

    it should "override blob meta-data" in {
      val blobClient = client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient
      createTestBlob(containerName, blobName, blobContent, Map("testKey" -> "testValue"))
      blobClient.setMetadata(getMetadataMap(Map("testKey2" -> "testValue2")))
      blobClient.getProperties.getMetadata.size() shouldBe 1
      blobClient.getProperties.getMetadata.get("testKey2") should be("testValue2")
    }

    it should "reset blob meta-data" in {
      val blobClient = client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient
      createTestBlob(containerName, blobName, blobContent, Map("testKey" -> "testValue"))
      blobClient.getProperties.getMetadata.size() shouldBe 1
      blobClient.setMetadata(getMetadataMap(Map()))
      blobClient.getProperties.getMetadata.size() shouldBe 0
    }
  }

  def getMetadataMap(map:Map[String, String]): java.util.Map[String, String] = {
    map.map{ case (k, v) => k -> v }.asJava
  }
}
