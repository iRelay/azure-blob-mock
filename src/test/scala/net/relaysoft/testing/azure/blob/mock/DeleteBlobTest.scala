package net.relaysoft.testing.azure.blob.mock

import com.azure.storage.blob.models.BlobStorageException

import scala.jdk.CollectionConverters.IterableHasAsScala

class DeleteBlobTest extends AzureBlobMockTest {

  val containerName = "test"

  override def beforeEach = {
    super.beforeEach()
    createContainer(containerName)
  }

  override def behaviour(fixture: => Fixture): Unit = {

    val client = fixture.client
    val blobName = "testBlob"
    val blobContent = "blob content".getBytes("UTF-8")

    it should "delete existing blob" in {
      createTestBlob(containerName, blobName, blobContent)
      client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient.delete()
      client.getBlobContainerClient(containerName).listBlobs().asScala.toList.size shouldBe(0)
    }

    it should "not delete non-existing blob" in {
      assertThrows[BlobStorageException] {
        client.getBlobContainerClient(containerName).getBlobClient(blobName).delete()
      }
    }
  }


}
