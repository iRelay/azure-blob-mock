package net.relaysoft.testing.azure.blob.mock

import scala.jdk.CollectionConverters._

class ListBlobsTest extends AzureBlobMockTest {

  val containerName = "test"

  override def beforeEach = {
    super.beforeEach()
    createContainer(containerName)
  }

  override def behaviour(fixture: => Fixture) = {

    val client = fixture.client

    it should "list empty container" in {
      client.getBlobContainerClient(containerName).listBlobs().asScala.toList.size shouldBe 0
    }

    it should "list existing blobs from container" in {
      val firstBlobName = "first/blob"
      val firstData = "first data"
      val secondBlobName = "second/blob"
      val secondData = "first data"
      createTestBlob(containerName, firstBlobName, firstData.getBytes("UTF-8"));
      createTestBlob(containerName, secondBlobName, secondData.getBytes("UTF-8"));
      val result = client.getBlobContainerClient(containerName).listBlobs().asScala.toList
      result.size shouldBe 2
      val names = result.map(blob => blob.getName).sorted
      names shouldEqual List(firstBlobName, secondBlobName).sorted
    }

    it should "list existing blobs from container with given prefix" in {
      val firstBlobName = "first/blob"
      val firstData = "first data"
      val secondBlobName = "second/blob"
      val secondData = "first data"
      createTestBlob(containerName, firstBlobName, firstData.getBytes("UTF-8"));
      createTestBlob(containerName, secondBlobName, secondData.getBytes("UTF-8"));
      val result = client.getBlobContainerClient(containerName).listBlobsByHierarchy("first/").asScala.toList
      result.size shouldBe 1
      val names = result.map(blob => blob.getName).sorted
      names shouldEqual List(firstBlobName).sorted
    }

  }

}
