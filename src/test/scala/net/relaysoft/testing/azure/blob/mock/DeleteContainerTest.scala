package net.relaysoft.testing.azure.blob.mock

import com.azure.storage.blob.models.BlobStorageException

import scala.jdk.CollectionConverters.IterableHasAsScala

class DeleteContainerTest extends AzureBlobMockTest {

  override def behaviour(fixture: => Fixture): Unit = {

    val client = fixture.client
    val containerName = "test"

    it should "delete existing container" in {
      client.createBlobContainer(containerName)
      client.listBlobContainers().asScala.toList.size shouldBe(1)
      client.deleteBlobContainer(containerName)
      client.listBlobContainers().asScala.toList.size shouldBe(0)
    }

    it should "not delete non-existing container" in {
      assertThrows[BlobStorageException] {
        client.deleteBlobContainer(containerName)
      }
    }
  }

}
