package net.relaysoft.testing.azure.blob.mock

import com.azure.storage.blob.BlobContainerClient
import com.azure.storage.blob.models.BlobStorageException

class CreateContainerTest extends AzureBlobMockTest {

  override def behaviour(fixture: => Fixture): Unit = {

    val client = fixture.client
    val containerName = "test"

    it should "create new container" in {
      val result = client.createBlobContainer(containerName)
      result.getBlobContainerName shouldBe containerName
    }

    it should "not create duplicate container" in {
      client.createBlobContainer(containerName)
      assertThrows[BlobStorageException] {
        client.createBlobContainer(containerName)
      }
    }
  }
}
