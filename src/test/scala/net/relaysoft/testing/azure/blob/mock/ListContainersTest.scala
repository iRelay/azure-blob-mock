package net.relaysoft.testing.azure.blob.mock

import scala.jdk.CollectionConverters._

class ListContainersTest extends AzureBlobMockTest {
  override def behaviour(fixture: => Fixture) = {

    val client = fixture.client

    it should "list empty account" in {
      client.listBlobContainers().asScala.toList.size shouldBe 0
    }

    it should "list existing containers from account" in {
      val firstContainerName = "firstcontainer"
      val secondContainerName = "secondcontainer"
      client.createBlobContainer(firstContainerName);
      client.createBlobContainer(secondContainerName);
      val result = client.listBlobContainers().asScala.toList
      result.size shouldBe 2
      val names = result.map(container => container.getName).sorted
      names shouldEqual List(firstContainerName, secondContainerName).sorted
    }

  }

}
