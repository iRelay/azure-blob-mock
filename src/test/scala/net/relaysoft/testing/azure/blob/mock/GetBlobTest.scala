package net.relaysoft.testing.azure.blob.mock

import com.azure.storage.blob.models.BlobStorageException

import java.io.{BufferedOutputStream, ByteArrayInputStream, InputStream, OutputStream}
import scala.collection.concurrent.TrieMap

class GetBlobTest extends AzureBlobMockTest {

  val containerName = "testContainer"

  override def beforeEach = {
    super.beforeEach()
    createContainer(containerName)
  }

  override def behaviour(fixture: => Fixture) = {

    val client = fixture.client
    val blobName = "testBlob"
    val blobContent = "blob content".getBytes("UTF-8")

    it should "get blob properties" in {
      val original = createTestBlob(containerName, blobName, blobContent)
      val props = client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient.getProperties
      props.getETag should be(original.getETag)
      props.getLastModified should be(original.getLastModified)
      props.getBlobSize should be(blobContent.length)
    }

    it should "fail to get non-existing blob input stream" in {
      assertThrows[BlobStorageException] {
        client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient.openInputStream()
      }
    }

    it should "get blob input stream" in {
      createTestBlob(containerName, blobName, blobContent)
      val is = client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient.openInputStream()
      val result = LazyList.continually(is.read).takeWhile(_ != -1).map(_.toByte).toArray
      result should be(blobContent)
    }
  }
}
