package net.relaysoft.testing.azure.blob.mock

import com.azure.storage.blob.models.BlobStorageException

class GetBlobMetadataTest extends AzureBlobMockTest {

  val containerName = "testContainer"

  override def beforeEach: Unit = {
    super.beforeEach()
    createContainer(containerName)
  }

  override def behaviour(fixture: => Fixture): Unit = {

    val client = fixture.client
    val blobName = "testBlob"
    val blobContent = "blob content".getBytes("UTF-8")

    it should "fail to get non-existing blob meta-data" in {
      assertThrows[BlobStorageException] {
        client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient.getProperties
      }
    }

    it should "get blob meta-data" in {
      createTestBlob(containerName, blobName, blobContent, Map("testKey" -> "testValue"))
      val metadata = client.getBlobContainerClient(containerName).getBlobClient(blobName).getBlockBlobClient
        .getProperties.getMetadata
      metadata.size() shouldBe 1
      metadata.get("testKey") should be("testValue")
    }
  }
}
