# Azure Blob Mock

[![Maven Central](https://img.shields.io/maven-central/v/net.relaysoft.commons/azure-blob-mock.svg?style=flat-square)](http://search.maven.org/#artifactdetails%7Cnet.relaysoft.commons%7Cazure-blob-mock%7C1.0.0%7Cjar)

## Overview

Azure Blob mock is a web service implementing Azure blob service API, which can be used for local testing of your code using Blob storage but without hitting real Blob storage REST endpoints.

Implemented API methods:

Account methods:

* List Containers

Container methods:

* Create Container
* Delete Container
* Get Container Properties
* List Blobs

Blob methods:

* Delete Blob
* Get Blob
* Get Blob Metadata
* Get Blob Properties
* Put Blob
* Set Blob Metadata

Blob mock API operations does not require nor check request credentials. Storage credentials headers are skipped.

## Installation

Attach Azure Blob mock into your project as Maven test dependency.

```xml
<dependency>
    <groupId>net.relaysoft.commons</groupId>
    <artifactId>azure-blob-mock</artifactId>
    <version>1.0.0</version>
    <scope>test</scope>
</dependency>
```

## Usage

Point your Azure Blob container client endpoint to a localhost e.g. `http://127.0.0.1:10000/devstoreaccount1` and it 
should work out-of-the box against blob mock instance. Port can be selected freely open any open ports.

It is important that endpoint path equals to `devstoreaccount1`. Endpoint path cannot be configured. This is because
Microsoft [Azurite](https://hub.docker.com/_/microsoft-azure-storage-azurite) docker image works with same endpoint URL,
which enables easy change between Blob mock library and Azurite container in case you need to test with more capable
Azure Storage API compatible emulator.

Azure blob mock currently works only with in-memory provider which keep everything in RAM. All the data you've uploaded 
to Azure Blob mock will be wiped on shutdown.

### Java example

```java
/*
 * Create Azure Blob mock which listens port 10000 and start it.
 */
AzureBlobMock azureBlobMock = new AzureBlobMock.Builder()
        .withPort(10000)
        .withInMemoryBackend()
        .build();
azureBlobMock.start();

/*
 * Create Blob container client instance. 
 */
BlobContainerClient client = new BlobServiceClientBuilder()
        .endpoint("http://127.0.0.1:10000/devstoreaccount1")
        .credential(new StorageSharedKeyCredential("devstoreaccount1", "<Base64 encoded account key>"))
        .buildClient()
        .getBlobContainerClient("testcontainer");

/*
 * Create test container       
 */
client.create();

/*
 * Create Blob block client and used it for creating new blob into test container       
 */
BlockBlobClient blockClient = client.getBlobClient("testblob").getBlockBlobClient();
byte[] data = "test content".getBytes(StandardCharsets.UTF_8);
blockClient.upload(new ByteArrayInputStream(data), data.length);

/*
 * Finally shutdown the mock       
 */
azureBlobMock.shutdown();
```

### Scala example

```scala
/*
 * Create Azure Blob mock which listens port 10000 and start it.
 */
val azureBlobMock = (port = 10000)
azureBlobMock.start

/*
 * Create Blob container client instance. 
 */
val client = new BlobServiceClientBuilder()
        .endpoint("http://127.0.0.1:10000/devstoreaccount1")
        .credential(new StorageSharedKeyCredential("devstoreaccount1", "<Base64 encoded account key>"))
        .buildClient()
        .getBlobContainerClient("testcontainer")

/*
 * Create test container       
 */
client.create

/*
 * Create Blob block client and used it for creating new blob into test container       
 */
val blockClient = client.getBlobClient("testblob").getBlockBlobClient
byte[] data = "test content".getBytes(StandardCharsets.UTF_8)
blockClient.upload(new ByteArrayInputStream(data), data.length)

/*
 * Finally shutdown the mock       
 */
azureBlobMock.shutdown
```

## Getting Started with development

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Java version 8 or newer

## Running the tests

Project unit tests are executed within Maven _test_.

Unit tests:

    mvn test

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on the process for submitting pull requests to us.

## Versioning

Uses [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/irelay/data-manager/tags).

## Authors

* **Tapani Leskinen** - [relaysoft.net](https://relaysoft.net)

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details

## Disclaimer

This library is developed and managed by the open-source community on GitLab. It is not part of Azure storage account 
products and is not supported under any Microsoft standard support program or service. The library is provided AS IS 
without warranty of any kind. For any issues, visit the GitLab repository.